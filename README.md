# webdock3

This is a series of docking notebooks that derived from https://github.com/quantaosun/labodock 

- Two of the notebooks are borrowed from https://github.com/quantaosun/labodock
- Three of notebooks are modified version for different purposes.
- Please cite the original paper if used in your publications


